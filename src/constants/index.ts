import type { SortOption } from "@/interfaces/ISortOption"

export const LIMIT = 10

export const SORT_OPTIONS: SortOption[] = [
  { name: "Sort by id", value: "id" },
  { name: "Sort by title", value: "title" },
  { name: "Sort by description", value: "body" }
]
