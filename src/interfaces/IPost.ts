export interface Post {
  id: number
  title: string
  body: string
}

export interface CreatePostDto {
  title: string
  body: string
}
