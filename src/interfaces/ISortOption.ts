export type SortingType = "id" | "title" | "body"

export interface SortOption {
  name: string
  value: SortingType
}
