export interface Sort {
  empty: boolean
  sorted: boolean
  unsorted: boolean
}

export interface Page<T> {
  content: Array<T>
  pageable: {
    pageNumber: number
    pageSize: number
    sort: Array<Sort>
    offset: number
    paged: boolean
    unpaged: boolean
  }
  totalPages: number
  totalElements: number
  first: boolean
  last: boolean
  numberOfElements: number
  empty: boolean
}
