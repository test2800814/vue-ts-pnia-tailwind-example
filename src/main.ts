import "@/assets/css/main.css"

import { createApp } from "vue"
import { createPinia } from "pinia"
import App from "@/App.vue"
import VFocus from "@/directives/VFocus"
import VIntersection from "@/directives/VIntersection"
import { vueKeycloak } from "@josempgon/vue-keycloak"
import router from "./router"

const app = createApp(App)

app.use(createPinia())
app.use(router)

app.use(vueKeycloak, {
  config: {
    url: import.meta.env.VITE_KEYCLOAK_URL_LOCAL ?? import.meta.env.VITE_KEYCLOAK_URL,
    realm: import.meta.env.VITE_KEYCLOAK_REALM_LOCAL ?? import.meta.env.VITE_KEYCLOAK_REALM,
    clientId: import.meta.env.VITE_KEYCLOAK_CLIENT_LOCAL ?? import.meta.env.VITE_KEYCLOAK_CLIENT
  },
  initOptions: {
    onLoad: "check-sso",
    silentCheckSsoRedirectUri: `${window.location.origin}/silent-check-sso.html`,
    checkLoginIframe: false
  }
})

app.directive("focus", VFocus)
app.directive("intersection", VIntersection)

app.mount("#app")
