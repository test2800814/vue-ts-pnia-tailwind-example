import type { SortingType } from "@/interfaces/ISortOption"
import type { Page } from "@/interfaces/IPage"
import type { CreatePostDto, Post } from "@/interfaces/IPost"
import useAxios from "@/hooks/useAxios"

const { axiosInstance } = useAxios()

export async function createPostRequest(post: CreatePostDto): Promise<Post> {
  const { data: createdPost } = await axiosInstance.post("/posts", post)

  return createdPost
}

interface GetPostsQueryParams {
  pageNumber: number
  sort: SortingType
  size: number
  search: string
}

export async function getPostsPageRequest(params: GetPostsQueryParams): Promise<Page<Post>> {
  const queryParams = createQueryParams(params)

  const { data: page } = await axiosInstance.get("/posts", {
    params: queryParams
  })

  return page
}

function createQueryParams({ pageNumber, size, sort, search }: GetPostsQueryParams): URLSearchParams {
  const queryParams = new URLSearchParams()

  if (pageNumber) {
    queryParams.append("page", pageNumber.toString())
  }

  if (size) {
    queryParams.append("size", size.toString())
  }

  if (sort) {
    queryParams.append("sort", sort)
    if (sort !== "id") {
      queryParams.append("sort", "id")
    }
  }

  if (search) {
    queryParams.append("search", search)
  }

  return queryParams
}

export async function getPostRequest(id: number): Promise<Post> {
  const { data: post } = await axiosInstance.get(`/posts/${id}`)

  return post
}

export async function updatePostRequest(post: Post): Promise<Post> {
  const { data: updatedPost } = await axiosInstance.put("/posts", post)

  return updatedPost
}

export async function removePostRequest(id: number): Promise<void> {
  await axiosInstance.delete(`/posts/${id}`)
}
