import type { DirectiveBinding } from "vue"

export default {
  mounted(el: HTMLElement, binding: DirectiveBinding<boolean>): void {
    if (binding.value) {
      el.focus()
    }
  }
}
