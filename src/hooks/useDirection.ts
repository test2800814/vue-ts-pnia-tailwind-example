import type { Ref } from "vue"
import { ref, watch } from "vue"
import type { UseSwipeDirection } from "@vueuse/core"
import { onKeyStroke, useSwipe } from "@vueuse/core"

export type Direction = Exclude<UseSwipeDirection, "none">

interface Hook {
  direction: Ref<Direction>
  forbiddenDirection: Ref<Direction>
}

export default function useDirection(
  initialDirection: Direction,
  initialForbiddenDirection: Direction,
  swipeElementRef: Ref
): Hook {
  const direction = ref<Direction>(initialDirection)
  const forbiddenDirection = ref<Direction>(initialForbiddenDirection)

  onKeyStroke(["ArrowUp", "w"], (e) => {
    e.preventDefault()
    e.stopPropagation()

    setDirection("up")
  })

  onKeyStroke(["ArrowRight", "d"], (e) => {
    e.preventDefault()
    e.stopPropagation()

    setDirection("right")
  })

  onKeyStroke(["ArrowDown", "s"], (e) => {
    e.preventDefault()
    e.stopPropagation()

    setDirection("down")
  })

  onKeyStroke(["ArrowLeft", "a"], (e) => {
    e.preventDefault()
    e.stopPropagation()

    setDirection("left")
  })

  const { direction: directionMobile } = useSwipe(swipeElementRef)

  watch(directionMobile, (newValue) => {
    if (newValue === "none") {
      return
    }

    setDirection(newValue)
  })

  function setDirection(value: Direction): void {
    if (value === forbiddenDirection.value) {
      return
    }

    direction.value = value
  }

  return {
    direction,
    forbiddenDirection
  }
}
