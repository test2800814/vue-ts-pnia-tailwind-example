import type { AxiosInstance } from "axios"
import axios from "axios"

interface Hook {
  axiosInstance: AxiosInstance
  setAuthorizationHeader: (token: string) => void
  setTempUserId: (tempUserId: string) => void
}

const BASE_URL = import.meta.env.VITE_BACK_SERVER_URL_LOCAL ?? import.meta.env.VITE_BACK_SERVER_URL
const axiosInstance = axios.create({
  baseURL: BASE_URL,
  timeout: 60000
})

export default function useAxios(): Hook {
  function setAuthorizationHeader(token: string): void {
    axiosInstance.defaults.headers.common["Temp-User-Id"] = null
    axiosInstance.defaults.headers.common["Authorization"] = `Bearer ${token}`
  }

  function setTempUserId(tempUserId: string): void {
    axiosInstance.defaults.headers.common["Authorization"] = null
    axiosInstance.defaults.headers.common["Temp-User-Id"] = tempUserId
  }

  return { axiosInstance, setAuthorizationHeader, setTempUserId }
}
