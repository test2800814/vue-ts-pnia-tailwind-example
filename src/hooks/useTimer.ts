import type { Ref } from "vue"
import { ref } from "vue"

interface Hook {
  now: Ref<Date>
  start: () => void
  stop: () => void
  timeout: Ref<number>
}

export default function useTimer(initialTimeout: number): Hook {
  const timeout = ref(initialTimeout)
  const timer = ref()
  const now = ref(new Date())

  function updateDate(): void {
    now.value = new Date()
  }

  function start(): void {
    timer.value = setInterval(updateDate, timeout.value)
  }

  function stop(): void {
    clearInterval(timer.value)
  }

  return {
    now,
    start,
    stop,
    timeout
  }
}
