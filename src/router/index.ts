import { createRouter, createWebHistory } from "vue-router"
import HomeView from "@/views/HomeView.vue"
import PostsBaseView from "@/views/posts/PostsBaseView.vue"
import PostListView from "@/views/posts/PostListView.vue"
import PostView from "@/views/posts/PostView.vue"
import AboutView from "@/views/AboutView.vue"
import SnakeView from "@/views/SnakeView.vue"

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      name: "Home",
      path: "/",
      component: HomeView
    },
    {
      name: "Posts",
      path: "/posts",
      component: PostsBaseView,
      children: [
        { name: "PostList", path: "", component: PostListView },
        { name: "Post", path: ":id", component: PostView }
      ]
    },
    {
      name: "About",
      path: "/about",
      component: AboutView
    },
    {
      name: "Snake",
      path: "/snake",
      component: SnakeView
    }
  ]
})

export default router
