import { defineStore } from "pinia"
import { ref, watch } from "vue"
import type { CreatePostDto, Post } from "@/interfaces/IPost"
import { LIMIT } from "@/constants"
import type { SortingType } from "@/interfaces/ISortOption"
import {
  createPostRequest,
  getPostRequest,
  getPostsPageRequest,
  removePostRequest,
  updatePostRequest
} from "@/api/postsService"

export const usePostsStore = defineStore("posts", () => {
  const currentPage = ref(0)
  const selectedSort = ref<SortingType>("id")
  const searchQuery = ref("")
  const posts = ref<Post[]>([])
  const isSearching = ref(false)
  const isFetching = ref(false)

  async function createPost(createPostDto: CreatePostDto): Promise<void> {
    const createdPost = await createPostRequest(createPostDto)

    posts.value.push(createdPost)
  }

  async function fetchPosts(): Promise<void> {
    isFetching.value = true

    const { content: newPosts } = await getPostsPageRequest({
      pageNumber: currentPage.value,
      sort: selectedSort.value,
      size: LIMIT,
      search: searchQuery.value
    })

    posts.value = newPosts
    isFetching.value = false
  }

  async function fetchNextPostsPage(): Promise<void> {
    if (isFetching.value) {
      return
    }

    const {
      content: newPosts,
      last,
      empty
    } = await getPostsPageRequest({
      pageNumber: currentPage.value + 1,
      sort: selectedSort.value,
      size: LIMIT,
      search: searchQuery.value
    })

    if (last && empty) {
      return
    }

    posts.value = [...posts.value, ...newPosts]
    currentPage.value++
  }

  async function fetchPostById(id: number): Promise<void> {
    const index = posts.value.findIndex((post) => post.id === id)
    try {
      const post = await getPostRequest(id)

      if (index === -1) {
        posts.value.push(post)
      } else {
        posts.value[index] = post
      }
    } catch (e) {
      if (index !== -1) {
        posts.value = posts.value.filter((it) => it.id !== id)
      }
    }
  }

  async function removePost(id: number): Promise<void> {
    await removePostRequest(id)

    posts.value = posts.value.filter((it) => it.id !== id)
  }

  async function selectSort(sort: SortingType): Promise<void> {
    const { content: newPosts } = await getPostsPageRequest({
      pageNumber: 0,
      sort: sort,
      size: LIMIT,
      search: searchQuery.value
    })

    posts.value = newPosts
    currentPage.value = 0
    selectedSort.value = sort
  }

  async function search(): Promise<void> {
    const { content: newPosts } = await getPostsPageRequest({
      pageNumber: 0,
      sort: selectedSort.value,
      size: LIMIT,
      search: searchQuery.value
    })

    posts.value = newPosts
    currentPage.value = 0
  }

  function processSearch(newValue: string): void {
    search().then(() => {
      if (searchQuery.value !== newValue) {
        processSearch(searchQuery.value)
      }
      isSearching.value = false
    })
  }

  function startSearching(): void {
    if (!isSearching.value) {
      isSearching.value = true
      processSearch(searchQuery.value)
    }
  }

  watch(searchQuery, startSearching)

  async function updatePost(postUpdateDto: Post): Promise<void> {
    try {
      const updatedPost = await updatePostRequest(postUpdateDto)

      const index = posts.value.findIndex((post) => post.id === postUpdateDto.id)
      if (index === -1) {
        posts.value.push(updatedPost)
      } else {
        posts.value[index] = updatedPost
      }
    } catch (e) {
      await fetchPostById(postUpdateDto.id)
    }
  }

  return {
    // ***State***
    currentPage,
    selectedSort,
    searchQuery,
    posts,
    isSearching,

    // ***Actions***
    createPost,
    fetchPosts,
    fetchNextPostsPage,
    fetchPostById,
    removePost,
    selectSort,
    updatePost,
    startSearching
  }
})
