import { ref } from "vue"
import { defineStore } from "pinia"
import { v4 } from "uuid"

export const useTempUserStore = defineStore("tempUserStore", () => {
  const tempUserId = ref(localStorage.getItem("tempUserId"))

  function generateTempUserId(): void {
    const newTempUserId = v4()
    localStorage.setItem("tempUserId", newTempUserId)
    tempUserId.value = newTempUserId
  }

  function clearTempUserId(): void {
    localStorage.removeItem("tempUserId")
    tempUserId.value = null
  }

  return { tempUserId, generateTempUserId, clearTempUserId }
})
